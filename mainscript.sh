#!/bin/bash
# author: Elifian#0761

#var
bold=$(tput bold)
italic=$(tput sitm)
normal=$(tput sgr0)
red_color=$(tput setaf 1)

#важная функция
cas(){
	clear
	REPLY=
}

#защита от дурачка
typing_protection(){
	echo "${red_color}${bold}Введено некорректное значение, повторите попытку${normal}"
	sleep 1
}

#обновление ключей арча
key_updates(){
	echo "Обновление ключей"
	sudo pacman -Sy archlinux-keyring --noconfirm
}

#настройка зеркал
reflector(){
	echo "Настройка reflector"
	sudo pacman -S --needed reflector --noconfirm
	systemctl stop reflector
	systemctl disable reflector
	sudo reflector --verbose --protocol https --latest 10 --sort rate --save /etc/pacman.d/mirrorlist
	echo "${bold}Зеркала archlinux успешно настроены!${normal}"
	sleep 1
}

#обновление системы
system_update(){
	echo "Обновление системы"
	sudo pacman -Syu --noconfirm
}

makepkg_setup(){
	makepkgconf="/etc/makepkg.conf"
	if [[ $(grep 'CFLAGS="-march=native -mtune=native -O2' ${makepkgconf}) ]]; then
		:
	else
	    sed -i 's/-march=x86-64 -mtune=generic/-march=native -mtune=native/g' ${makepkgconf}
	    sed -i '/LDFLAGS/d' ${makepkgconf}
	    sed -i '/LTOFLAGS/d' ${makepkgconf}
	    sed -i '/CXXFLAGS="$CFLAGS.*"/a RUSTFLAGS="-C opt-level=3"' ${makepkgconf}
	    sed -i '/#MAKEFLAGS="-j2"/d' ${makepkgconf}
	    sed -i '/RUSTFLAGS="-C opt-level=3"/a MAKEFLAGS="-j$(nproc) -l$(nproc)"' ${makepkgconf}
	    sed -i '/MAKEFLAGS="-j$(nproc) -l$(nproc)"/a OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug !lto)' ${makepkgconf}
	    grep -v "#" ${makepkgconf} | grep "BUILDENV=(.*)" | sed -i 's/!ccache/ccache/g' ${makepkgconf}
	fi
}

grub_parameters(){
	sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=".*"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash rootfstype=btrfs raid=noautodetect noibrs noibpb nopti nospectre_v2 nospectre_v1 l1tf=off nospec_store_bypass_disable no_stf_barrier mds=off tsx=on tsx_async_abort=off mitigations=off"/g' /etc/default/grub
	sudo sed -i 's/GRUB_TIMEOUT=".*"/GRUB_TIMEOUT="1"/g' /etc/default/grub
}

#установка репозиториев multilib и chaotic-aur
repositories(){
	echo "multilib и chaotic-aur"
	if grep "[[]" /etc/pacman.conf | grep "multilib" | grep -v "multilib-testing" | grep -v "#" > /dev/null; then
		:
	else
		sudo sed -i '$ a \\n[multilib]\nInclude = /etc/pacman.d/mirrorlist' /etc/pacman.conf
	fi
	if grep "chaotic-aur" /etc/pacman.conf; then
		:
	else
	  sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
	  sudo pacman-key --lsign-key FBA220DFC880C036
	  sudo pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
	  sudo sed -i '$ a \\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist' /etc/pacman.conf
	fi
}

#установка редакторов кода
editors(){
    echo "Какой редактор вы хотите установить? ${bold}(можно выбрать несколько)${normal}
1) nano
2) micro
3) vim
4) neovim
5) vscodium
6) VS CODE
Чтобы ничего не устанавливать просто нажмите ${bold}ENTER${normal}"
    read EDITOR
	if [[ $EDITOR =~ [1-6] ]]; then
    	if [[ $EDITOR =~ [1] ]]; then
    	    EDITOR1=nano
    	fi
		if [[ $EDITOR =~ [2] ]]; then
    	    EDITOR2=micro
    	fi
		if [[ $EDITOR =~ [3] ]]; then
    	    EDITOR3=vim
    	fi
		if [[ $EDITOR =~ [4] ]]; then
    	    EDITOR4=neovim
    	fi
		if [[ $EDITOR =~ [5] ]]; then
    	    EDITOR5=vscodium
    	fi
		if [[ $EDITOR =~ [6] ]]; then
    	    EDITOR6=visual-studio-code-bin
    	fi
		EDITOR0="${EDITOR1} ${EDITOR2} ${EDITOR3} ${EDITOR4} ${EDITOR5} ${EDITOR6}"
        sudo pacman -Sy --needed --noconfirm ${EDITOR0}
	elif [[ $EDITOR = "" ]]; then
        :
	else
		typing_protection
		editors
	fi
}

#установка моих алиасов
aliases(){
	wget -P /home/$USER/.cache/arch-after-installing/ https://cdn.discordapp.com/attachments/988022576448159775/988022781910351892/aliases.sh
	mv /home/$USER/.cache/arch-after-installing/aliases.sh /home/$USER/.aliases.sh
	rm /home/$USER/.bashrc
	wget -P /home/$USER/.cache/arch-after-installing/ https://cdn.discordapp.com/attachments/988022576448159775/988022781717393488/bashrc
	mv home/$USER/.cache/arch-after-installing/bashrc /home/$USER/.bashrc
}

#настройка ссд/хдд
io_scheduler(){
	sudo touch /etc/udev/rules.d/60-ioschedulers.rules
	echo '# set scheduler for NVMe
ACTION=="add|change", KERNEL=="nvme[0-9]n[0-9]", ATTR{queue/scheduler}="none"
# set scheduler for SSD and eMMC
ACTION=="add|change", KERNEL=="sd[a-z]*|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
# set scheduler for rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]*", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
' | sudo tee -a /etc/udev/rules.d/60-ioschedulers.rules
}

#служба из ядра zen для стокового
cfs_zen_tweaks(){
		yay -S cfs-zen-tweaks --needed --noconfirm
		systemctl enable --now set-cfs-tweaks.service
}

#Настройка fancontrol
fancontrol(){
	yay -S --needed --noconfirm lm_sensors
	sudo rm /etc/conf.d/lm_sensors
	echo "Y" | sudo sensors-detect >/dev/null
	sudo systemctl enable --now lm_sensors
	sudo systemctl restart lm_sensors
	
	sudo rm /etc/fancontrol
	echo '# Configuration file generated by pwmconfig, changes will be lost
INTERVAL=10
DEVPATH=hwmon1=devices/platform/coretemp.0 hwmon2=devices/platform/nct6775.656
DEVNAME=hwmon1=coretemp hwmon2=nct6776
FCTEMPS=hwmon2/pwm3=hwmon1/temp1_input hwmon2/pwm2=hwmon1/temp1_input hwmon2/pwm1=hwmon1/temp1_input
FCFANS=hwmon2/pwm3=hwmon2/fan2_input hwmon2/pwm2=hwmon2/fan2_input hwmon2/pwm1=hwmon2/fan2_input
MINTEMP=hwmon2/pwm3=20 hwmon2/pwm2=20 hwmon2/pwm1=20
MAXTEMP=hwmon2/pwm3=60 hwmon2/pwm2=60 hwmon2/pwm1=60
MINSTART=hwmon2/pwm3=180 hwmon2/pwm2=180 hwmon2/pwm1=180
MINSTOP=hwmon2/pwm3=100 hwmon2/pwm2=0 hwmon2/pwm1=100
MINPWM=hwmon2/pwm3=0 hwmon2/pwm1=0
MAXPWM=hwmon2/pwm3=255 hwmon2/pwm2=255 hwmon2/pwm1=255' | sudo tee -a /etc/fancontrol
	sudo systemctl enable --now fancontrol
	sudo systemctl restart fancontrol
}

#настройка колеса мыши
imwheel(){
	echo "Настройка imwheel"
	yay -S imwheel --needed --noconfirm
	echo "[Unit]
Description=IMWheel
Wants=display-manager.service
After=display-manager.service

[Service]
Type=simple
Environment=XAUTHORITY=%h/.Xauthority
ExecStart=/usr/bin/imwheel -b 45
ExecStop=/usr/bin/pkill imwheel
RemainAfterExit=yes

[Install]
WantedBy=graphical-session.target" > /home/$USER/.config/systemd/imwheel.service
	echo "# Speed up scrolling for the document viewer
".*"
    None, Up, Button4, 2
    None, Down, Button5, 2
".dolphin"
    None, Up, Button4, 1
    None, Down, Button5, 1" > /home/$USER/.imwheelrc
	systemctl enable --now --user imwheel
}

#настройка микрофона
microphone(){
	echo "Настройка микрофона"
	yay -S noisetorch-bin --needed --noconfirm
echo "[Desktop Entry]
Name=noisetorch
Comment=noisetorch
Exec=noisetorch -i
Icon=noisetorch
Terminal=false
Type=Application
Categories=Utility;" > /home/$USER/.config/autostart/noisetorch.desktop
}

#исправление задержки капслока в X11
capslock(){
xkbcomp -xkb "$DISPLAY" - | sed 's#key <CAPS>.*#key <CAPS> {\
    repeat=no,\
    type[group1]="ALPHABETIC",\
    symbols[group1]=[ Caps_Lock, Caps_Lock],\
    actions[group1]=[ LockMods(modifiers=Lock),\
    Private(type=3,data[0]=1,data[1]=3,data[2]=3)]\
};\
#' | xkbcomp -w 0 - "$DISPLAY"
}

#исправление долгого выключения пк
fix_long_shutdown(){
	echo "Исправление долгой загрузки системы"
	sudo sed -i 's/#DumpCore=yes/DumpCore=no/g' /etc/systemd/system.conf
	sudo sed -i 's/#DefaultTimeoutStartSec=90s/DefaultTimeoutStartSec=05s/g' /etc/systemd/system.conf
	sudo sed -i 's/#DefaultTimeoutStopSec=90s/DefaultTimeoutStopSec=05ss/g' /etc/systemd/system.conf
}

#установка обязательных пакетов
soft(){
	echo "Установка необходимых приложений"
	sudo pacman -Sy --needed linux linux-headers linux-firmware linux-firmware-whence m4 git wget curl mpv neofetch noto-fonts-cjk noto-fonts-emoji ttf-joypixels ttf-dejavu ttf-font-awesome intel-ucode iucode-tool amd-ucode iucode-tool --noconfirm

    git clone https://aur.archlinux.org/yay-bin.git /home/$USER/.cache/yay-bin.git
    cd /home/$USER/.cache/yay-bin.git
    makepkg -si
}

#установка драйверов
drivers(){
	if [ -f /home/$USER/.cache/arch-after-installing/drivers/mainscript.sh ]; then
		sh /home/$USER/.cache/arch-after-installing/drivers/mainscript.sh
	else
		wget -P /home/$USER/.cache/arch-after-installing/drivers https://codeberg.org/Elifian/archlinux-drivers-script/raw/branch/main/mainscript.sh && sh /home/$USER/.cache/arch-after-installing/drivers/mainscript.sh
	fi
}

#установка ядра №2
kernel_2(){
	echo "Выберите ядро:
1 - Linux-zen
2 - Linux-xanmod-*
Чтобы ничего не устанавливать просто нажмите ${bold}ENTER${normal}"
	read -r -p "${bold}Выберите операцию (1-2): ${normal}" KERNEL_CHOICE
	if [[ $KERNEL_CHOICE =~ [1-2] ]]; then
		if [[ $KERNEL_CHOICE = [1] ]]; then
		    sudo pacman -Sy linux-zen linux-zen-headers --needed --noconfirm
			sudo mkinitcpio -P
			sudo grub-mkconfig -o /boot/grub/grub.cfg
    	fi
		if [[ $KERNEL_CHOICE = [2] ]]; then
		    sudo pacman -Sy linux-xanmod-edge linux-xanmod-edge-headers --needed --noconfirm
			sudo mkinitcpio -P
			sudo grub-mkconfig -o /boot/grub/grub.cfg
		fi
	elif [[ $KERNEL_CHOICE = "" ]]; then
		:
	else
		typing_protection
		kernel_2
	fi
}

#установка ядра №1
kernel(){
	echo "Вы хотите установить кастомное ядро?
1 - ДА
2 - НЕТ
Если выберите НЕТ, то установится стоковое
Чтобы ничего не устанавливать просто нажмите ${bold}ENTER${normal}"
	read -r -p "${bold}Выберите операцию (1-2): ${normal}" KERNEL
	if [[ $KERNEL =~ [1-2] ]]; then
		if [[ $KERNEL = [1] ]]; then
			kernel_2
		fi
		if [[ $KERNEL = [2] ]]; then
		    sudo pacman -Sy linux linux-headers --needed --noconfirm
			sudo mkinitcpio -P
			sudo grub-mkconfig -o /boot/grub/grub.cfg
		fi

	elif [[ $KERNEL = "" ]]; then
		:
	else
		typing_protection
		kernel
	fi
}

fstrim(){
	root_drive=$(mount | grep -w '/' | cut -d ' ' -f1 | tail -c 5 | cut -c -3)
	root_drive_check=$(cat /sys/block/$root_drive/queue/rotational)
	home_drive=$(mount | grep -w '/home' | cut -d ' ' -f1 | tail -c 5 | cut -c -3)
	home_drive_check=$(cat /sys/block/$home_drive/queue/rotational)
	if [[ root_drive_check = 0 ]]; then
		sudo fstrim -v /
	else
		:
	fi
	if [[ home_drive_check = 0 ]]; then
		sudo fstrim -v /home
	else
		:
	fi
}

#установка полезных служб
services(){
	echo "Установка служб для производительности системы ${bold}(Можно выбрать несколько)${normal}
0) Установить всё
1) ananicy-cpp - ${italic}Автоматическое назначение приоритетов программ. Улучшает отклик системы${normal}
2) nohang - ${italic}Слежение за памятью${normal}
3) irqbalance - ${italic}Распределение нагрузки по ядрам процессора${normal}
4) dbus-broker - ${italic}Улучшенный dbus. Дает лучшую работу с PCI${normal}
5) fstrim.timer - ${italic}Оптимизирует SSD. Без TRIM накопитель со временем теряет скорость чтения/записи${normal}
Чтобы ничего не устанавливать просто нажмите ${bold}ENTER${normal}"
read -r -p "${bold}Выберите операцию (0-5): ${normal}" PACKAGE
if [[ $PACKAGE =~ [0-5] ]]; then
	if [[ $PACKAGE =~ [0] ]]; then
		PACKAGE1=ananicy-cpp
		PACKAGE2=ananicy-rules-git
		PACKAGE3=nohang
		PACKAGE4=irqbalance
		PACKAGE5=dbus-broker
		PACKAGE6=fstrim.timer
	fi
	if [[ $PACKAGE =~ [1] ]]; then
	    PACKAGE1=ananicy-cpp
		PACKAGE2=ananicy-rules-git
	fi
	if [[ $PACKAGE =~ [2] ]]; then
	    PACKAGE3=nohang
	fi
	if [[ $PACKAGE =~ [3] ]]; then
	    PACKAGE4=irqbalance
	fi
	if [[ $PACKAGE =~ [4] ]]; then
	    PACKAGE5=dbus-broker
	fi
	if [[ $PACKAGE =~ [5] ]]; then
	    PACKAGE6=fstrim.timer
	fi
	if [[ $PACKAGE =~ [0-4] ]]; then
		PACKAGE0="${PACKAGE1} ${PACKAGE2} ${PACKAGE3} ${PACKAGE4} ${PACKAGE5}"
		sudo pacman -Sy --needed --noconfirm ${PACKAGE0}
		if [[ $PACKAGE1 = "ananicy-cpp" ]]; then
			SERVICE1=ananicy-cpp
		fi
		if [[ $PACKAGE3 = "nohang" ]]; then
			SERVICE2=nohang-desktop
		fi
		if [[ $PACKAGE4 = "irqbalance" ]]; then
			SERVICE3=irqbalance
		fi
		if [[ $PACKAGE5 = "dbus-broker" ]]; then
			SERVICE4=dbus-broker
		fi
		if [[ $PACKAGE6 = "fstrim.timer" ]]; then
			SERVICE5=fstrim.timer
			fstrim
		fi
		SERVICE0="${SERVICE1} ${SERVICE2} ${SERVICE3} ${SERVICE4} ${SERVICE5}"
    	systemctl enable --now ${SERVICE0}
	fi
elif [[ $PACKAGE = "" ]]; then
	:
else
	typing_protection
	services
fi
}

#настройка шрифтов
fonts(){
	echo "Настройка сглаживания шрифтов"
	sudo sed '12s/^#//' -i /etc/profile.d/freetype2.sh
	wget -P /home/$USER/.cache/arch-after-installing/ https://cdn.discordapp.com/attachments/988022576448159775/988022781507682354/local.conf
	sudo mv /home/$USER/.cache/arch-after-installing/local.conf /etc/fonts/
	echo -e "Xft.dpi: 96
	Xft.antialias: true
	Xft.hinting: true
	Xft.rgba: rgb
	Xft.autohint: false
	Xft.hintstyle: hintslight
	Xft.lcdfilter: lcddefault" >> ~/.Xresources
	sudo rm /etc/vconsole.conf
	sudo touch /etc/vconsole.conf
	echo -e "FONT=cyr-sun16
KEYMAP=us" | tee -a /etc/vconsole.conf
}

#установка звукового сервера
audio(){
	echo "Какой вы хотите установить звуковой сервер?
1) pipewire
2) pulseaudio
3) Ничего не устанавливать"
	read -r -p "${bold}Выберите операцию (1-2): ${normal}" SOUND_SERVERS
	if [[ "$SOUND_SERVERS" == 1 ]]; then
		sudo pacman -S --needed pipewire wireplumber pipewire-pulse pipewire-docs pipewire-alsa pipewire-jack lib32-pipewire lib32-pipewire-jack libpulse lib32-libpulse xdg-desktop-portal
		systemctl --user enable --now pipewire.service
	elif [[ "$SOUND_SERVERS" == 2 ]]; then
		sudo pacman -S pulseaudio pulseaudio-alsa alsa-utils lib32-libpulse xdg-desktop-portal
		pulseaudio -D
	elif [[ "$SOUND_SERVERS" == 3 ]]; then
		:
	else
		typing_protection
		audio
	fi
}

#установка ZSH №2
ZSH_2(){
		echo "Вы хотите установить тему powerlevel10k?"
	read -r -p "${bold}Выберите операцию (YyДд/NnНн): ${normal}" POWERLEVEL_INSTALL
	if [[ "$POWERLEVEL_INSTALL" = [Yy,Дд] ]]; then
		git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
		sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel10k/powerlevel10k"/g' /home/$USER/.zshrc
	elif [[ "$POWERLEVEL_INSTALL" = [Nn,Нн] ]]; then
		:
	else
		typing_protection
		ZSH_2
	fi
}

#установка ZSH №1
ZSH(){
	echo "Вы хотите установить ZSH и Oh-My-Zsh?"
	read -r -p "${bold}Выберите операцию (YyДд/NnНн): ${normal}" ZSH_INSTALL
	if [[ "$ZSH_INSTALL" == [Yy,Дд] ]]; then
		sudo pacman -Sy zsh --needed --noconfirm
		curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
		git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
		git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
		sed -i 's/plugins=(git)/plugins=(git zsh-syntax-highlighing zsh-autosuggestions)/g' /home/$USER/.zshrc
		ZSH_2
	elif [[ "$ZSH_INSTALL" == [Nn,Нн] ]]; then
		:
	else
		typing_protection
		ZSH
	fi
}

#установка браузера
browser(){
	echo "Какой браузер установить?
1) Google Chrome
2) Firefox
Чтобы ничего не устанавливать просто нажмите ${bold}ENTER${normal}"
	read -r -p "${bold}Выберите операцию (1-2): ${normal}" BROWSER
	if [[ "$BROWSER" =~ [1-2] ]]; then
		if [[ "$BROWSER" == 1 ]]; then
    	sudo pacman -Sy google-chrome --needed --noconfirm
		fi
		if [[ "$BROWSER" == 2 ]]; then
		sudo pacman -Sy firefox --needed --noconfirm
		fi
	elif [[ "$BROWSER" == "" ]]; then
		:
	else
		typing_protection
		browser
	fi
}

#установка wine
games(){
	echo "Что вы выберете для запуска Windows игр на Linux?
1) PortWINE
2) StartWine
Чтобы ничего не устанавливать просто нажмите ${bold}ENTER${normal}"
	read -r -p "${bold}Выберите операцию (1-2): ${normal}" GAMES
	if [[ "$GAMES" =~ [1-2] ]]; then
		if [[ "$GAMES" == 1 ]]; then
		  	sudo pacman -Syu --needed bash icoutils wget bubblewrap zstd cabextract bc tar openssl gamemode desktop-file-utils curl dbus freetype2 gdk-pixbuf2 ttf-font zenity lsb-release nss xorg-xrandr vulkan-driver vulkan-icd-loader lsof lib32-freetype2 lib32-libgl lib32-gcc-libs lib32-libx11 lib32-libxss lib32-alsa-plugins lib32-libgpg-error lib32-nss lib32-vulkan-driver lib32-vulkan-icd-loader lib32-gamemode lib32-openssl --noconfirm
		  	wget -c "https://github.com/Castro-Fidel/PortWINE/raw/master/portwine_install_script/PortProton_1.0" && sh PortProton_1.0 -y
		fi
		if [[ "$GAMES" == 2 ]]; then
			cd /home/$USER/Загрузки/
			wget https://cdn.discordapp.com/attachments/858478894989312040/978765476941414420/StartWine_v355
			chmod +x StartWine_v355 && sh StartWine_v355
		fi
	elif [[ "$GAMES" == "" ]]; then
		:	
	else
		typing_protection
		games
	fi
}

#установка граф. установщика pacman
pamac(){
	echo "Вы хотите установить графический pamac (pacman)?"
	read -r -p "${bold}Выберите операцию (YyДд/NnНн): ${normal}" PAMAC_INSTALL
	if [[ "$PAMAC_INSTALL" == [Yy,Дд] ]] && [[ "$AUR_HELPER" == 1 ]] ; then
	  yay -S pamac-aur archlinux-appstream-data --needed --noconfirm
	elif [[ "$PAMAC_INSTALL" == [Yy,Дд] ]] && [[ "$AUR_HELPER" == 2 ]] ; then
	  pikaur -S pamac-aur archlinux-appstream-data --needed --noconfirm
	elif [[ "$PAMAC_INSTALL" == [Nn,Нн] ]] ; then
		:
	else
		typing_protection
		pamac
	fi
}

#перезагрузка
reboot(){
	echo "Вы хотите перезагрузить систему?"
	read -r -p "${bold}Выберите операцию (YyДд/NnНн): ${normal}" SYSTEM_REBOOT
	if [[ "$SYSTEM_REBOOT" = [Yy,Дд] ]]; then
	  reboot
	elif [[ "$SYSTEM_REBOOT" = [Nn,Нн] ]]; then
	  :
	fi
}

#функция запуска
start(){
	clear
	echo "
█▀ █▀▀ ▀█▀ █ █ █▀█
▄█ ██▄  █  █▄█ █▀▀
"
	echo "Запустить весь скрипт или выбрать отдельные модули?
1) Запустить весь скрипт
2) Выбрать отдельно основные модули
3) Выбрать отдельно дополнительные модули
4) Выйти"
	read -r -p "${bold}Выберите операцию (1-4): ${normal}" TEXT
	if [[ "$TEXT" == [1] ]]; then
	    key_updates
	    system_update
		makepkg_setup
		grub_parameters
	    reflector
	    repositories
	    editors
		aliases
		io_scheduler
		fancontrol
		imwheel
		microphone
		capslock
		fix_long_shutdown
	    soft
		kernel
	    drivers
	    services
	    fonts
	    audio
	    ZSH
	    browser
	    pamac
	    reboot
	elif [[ "$TEXT" == [2] ]]; then
	clear
	PS3="
${bold}Выберите операцию (1-24): ${normal}"
	select main_modules in Выйти Перезагрузиться Обновить\ Ключи Обновление\ Системы Настройка\ makepkg.conf Настройка\ GRUB Настройка\ reflector Настройка\ Репозиториев Установка\ Редактора Настройка\ Aliases Настройка\ I/O\ scheduler Настройка\ fancontrol Настройка\ Колеса\ Мыши Настройка\ Микрофона Исправление\ Capslock Исправление\ Долгой\ Загрузки Установка\ Нужных\ Приложений Установка\ Драйверов Установка\ Ядра Настройка\ Служб Настройка\ Шрифтов Настройка\ Аудио Установка\ ZSH Установка\ pamac; do

	case $main_modules in
  		Обновить\ Ключи)
			key_updates
			sleep 1
			cas
			;;
		Настройка\ reflector)
			reflector
			sleep 1
			cas
			;;
		Обновление\ Системы)
			system_update
			sleep 1
			cas
			;;
		Настройка\ makepkg.conf)
			makepkg_setup
			sleep 1
			cas
			;;
		Настройка\ GRUB)
			grub_parameters
			sleep 1
			cas
			;;
		Настройка\ Репозиториев)
			repositories
			sleep 1
			cas
			;;
		Установка\ Редактора)
			editors
			sleep 1
			cas
			;;
		Настройка\ Aliases)
			aliases
			sleep 1
			cas
			;;
		Настройка\ I/O\ scheduler)
			io_scheduler
			sleep 1
			cas
			;;
		Настройка\ fancontrol)
			fancontrol
			sleep 1
			cas
			;;
		Настройка\ Колеса\ Мыши)
			imwheel
			sleep 1
			cas
			;;
		Настройка\ Микрофона)
			microphone
			sleep 1
			cas
			;;
		Исправление\ Capslock)
			capslock
			sleep 1
			cas
			;;
		Исправление\ Долгой\ Загрузки)
			fix_long_shutdown
			sleep 1
			cas
			;;
		Установка\ Нужных\ Приложений)
			soft
			sleep 1
			cas
			;;
		Установка\ Драйверов)
			drivers
			sleep 1
			cas
			;;
		Установка\ Ядра)
			kernel
			sleep 1
			cas
			;;
		Настройка\ Служб)
			services
			sleep 1
			cas
			;;
		Настройка\ Шрифтов)
			fonts
			sleep 1
			cas
			;;
		Настройка\ Аудио)
			audio
			sleep 1
			cas
			;;
		Установка\ ZSH)
			ZSH
			sleep 1
			cas
			;;
		Установка\ pamac)
			pamac
			sleep 1
			cas
			;;
		Перезагрузиться)
			reboot
			;;
		Выйти)
			start
			;;
    	*) 
    	  typing_protection;;
	esac
	done
	elif [[ "$TEXT" == [3] ]]; then
		clear
		PS3="
${bold}Выберите операцию (1-5): ${normal}"
	select additional_modules in Выйти Перезагрузиться Установка\ Браузера Установка\ PortWINE\ или\ StartWine Установка\ cfs-zen-tweaks; do
	case $additional_modules in
		Установка\ Браузера)
			browser
			sleep 1
			cas
			;;
		Установка\ PortWINE\ или\ StartWine)
			games
			sleep 1
			cas
			;;
		Установка\ cfs-zen-tweaks)
			cfs_zen_tweaks
			sleep 1
			cas
			;;
		Перезагрузиться)
			reboot
			;;
		Выйти)
			start
			;;
		*) 
    		typing_protection;;
	esac
	done
	elif [[ "$TEXT" == [4] ]]; then
		exit
	else
		typing_protection
		start
fi
}

#Запуск функции старта
start
